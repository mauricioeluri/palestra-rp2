package t1rp2.bruno;

public class ArtigoIndex {

    public static void main(String[] args) {
        int opcao;
        System.out.println("***PALESTRA***\n");
        do {
            opcao = ArtigoMetodos.menu();
            switch (opcao) {
                case 1:
                    ArtigoMetodos.incluir();
                    break;

                case 2:
                    ArtigoMetodos.pesquisa();
                    break;
            }
        } while (opcao != 0);
    }
}
