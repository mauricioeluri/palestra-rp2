package t1rp2.bruno;

public class Artigo {

    private String titulo;
    private int situacao;
    private String[] autor;
    private String[] instituicao;
    private String[] palavrachave;
    private String resumo;
    private String abstrato;

    public Artigo(String titulo, int situacao, String[] autor, String[] instituicao, String[] palavrachave, String resumo, String abstrato) {
        this.titulo = titulo;
        this.situacao = situacao;
        this.autor = autor;
        this.instituicao = instituicao;
        this.resumo = resumo;
        this.abstrato = abstrato;
        this.palavrachave = palavrachave;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getSituacao() {
        return situacao;
    }

    public void setSituacao(int situacao) {
        this.situacao = situacao;
    }

    public String[] getAutor() {
        return autor;
    }

    public void setAutor(String[] autor) {
        this.autor = autor;
    }

    public String[] getPalavrachave() {
        return palavrachave;
    }

    public void setPalavrachave(String[] palavrachave) {
        this.palavrachave = palavrachave;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getAbstrato() {
        return abstrato;
    }

    public void setAbstrato(String abstrato) {
        this.abstrato = abstrato;
    }

    public String[] getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(String[] instituicao) {
        this.instituicao = instituicao;
    }

}
