package t1rp2.bruno;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArtigoMetodos {

    public static List<Artigo> ListaArtigo = new ArrayList<>();

    public static String lerString(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        do {
            System.out.print("Digite " + nome);
            var = entrada.nextLine();
        } while (var.trim().equalsIgnoreCase(""));
        return var;

    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static int lerInteiro(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        int integer;
        do {
            System.out.print("Digite " + nome);
            var = entrada.nextLine().trim();
            if (isInteger(var) == false) {
                System.out.print("\nVocê digitou um valor inválido.\n");
            }
        } while (isInteger(var) == false);
        integer = Integer.parseInt(var);
        return integer;
    }

    public static String verificaSituacao(int valor) {
        String situacao = "Sob Avaliação";
        if (valor == 2) {
            situacao = "Aprovado";
        } else if (valor == 3) {
            situacao = "Reprovado";
        }
        return situacao;
    }

    public static String consultar(Artigo artigo) {
        String texto;
        String[] autores = artigo.getAutor();
        String[] inst = artigo.getInstituicao();
        String[] palavras = artigo.getPalavrachave();
        texto = "\nTítulo: " + artigo.getTitulo()
                + ";\nSituação: " + verificaSituacao(artigo.getSituacao());
        for (int i = 0; i < autores.length; i++) {
            texto += ";\n" + (i + 1) + "º Autor: " + autores[i] + ", " + inst[i];
        }
        texto += ";\nPalavras-Chave: ";
        for (String palavra : palavras) {
            texto += palavra + "; ";
        }
        texto
                += "\nResumo: " + artigo.getResumo()
                + ";\nAbstract: " + artigo.getAbstrato()
                + ";\n";
        return texto;
    }

    public static void pesquisa() {
        if (ListaArtigo.size() > 0) {
            String search = lerString("um texto para ser pesquisado: ");
            Artigo art;
            int resultados = 0;
            for (int i1 = 0; i1 < ListaArtigo.size(); i1++) {
                art = ListaArtigo.get(i1);
                String texto = consultar(art);
                if (texto.contains(search)) {
                    System.out.println(texto);
                    resultados++;
                }
            }
            if (resultados == 0) {
                System.out.println("Nenhum resultado foi encontrado");
            }
        } else {
            System.out.println("Não existem artigos cadastrados.\n");
        }
    }

    public static int menu() {
        int opcao;
        do {
            opcao = lerInteiro("uma opção:\n[1] Incluir artigo\n[2] Consultar artigo\n[0] Sair\nDigite uma opção: ");
            if (opcao < 0 || opcao > 2) {
                System.out.print("Você digitou uma opção inválida.\n\n");
            }
        } while (opcao < 0 && opcao > 2);
        return opcao;
    }
    
    public static void incluir() {
        String titulo, resumo, abstrato;
        int autores, palavras, situacao;

        /*=======================================================================================================*/
        System.out.println();
        titulo = lerString("o título: ");
        
        do {
            System.out.println("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado");
            situacao = lerInteiro("a situação: ");
            if (situacao <= 0 || situacao > 3) {
                System.out.print("\nVocê digitou um valor inválido.\n");
            }
        } while (situacao <= 0 || situacao > 3);

        /*=======================================================================================================*/
        do {
            autores = lerInteiro("o número de autores que você deseja inserir (1 a 8): ");
            if (autores <= 0 || autores > 8) {
                System.out.print("\nVocê deve adicionar entre 1 a 8 autores: ");
            }
        } while (autores <= 0 || autores > 8);
        String autor[] = new String[autores];
        for (int i = 0; i < autores; i++) {
            autor[i] = lerString("o " + (i + 1) + "º autor: ");
        }

        /*=======================================================================================================*/
        String instituicao[] = new String[autores];
        for (int x = 0; x < autores; x++) {
            instituicao[x] = lerString("a instituição referente ao " + autor[x] + ": ");
        }

        /*=======================================================================================================*/
        do {
            palavras = lerInteiro("o número de palavras-chave que você deseja inserir: ");
            if (palavras <= 0 || palavras > 4) {
                System.out.print("\nVocê digitou um valor inválido, você pode adicionar de 1 até 4 palavras-chave. \nDigite de novo: ");
            }
        } while (palavras <= 0 || palavras > 4);
        String palavrachave[] = new String[palavras];
        for (int z = 0; z < palavras; z++) {
            palavrachave[z] = lerString("a " + (z + 1) + "º palavra-chave: ");
        }

        /*=======================================================================================================*/
        resumo = lerString("o resumo: ");
        abstrato = lerString("o abstract: ");

        Artigo artigo = new Artigo(titulo, situacao, autor, instituicao, palavrachave, resumo, abstrato);
        ListaArtigo.add(artigo);
        System.out.println("Artigo adicionado com sucesso!\n");
    }
}
