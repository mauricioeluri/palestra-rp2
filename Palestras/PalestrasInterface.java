package t1rp2.Palestras;

import static bruno.ArtigoMetodos.lerInteiro;
import static bruno.ArtigoMetodos.lerString;
import static t1rp2.Palestras.Palestra.consultar;
import static t1rp2.Palestras.PalestrasMetodos.ListaPalestras;

public class PalestrasInterface {

//    public static String pesquisa(String pesquisa) {
//        String texto = "";
//        if (ListaPalestras.size() > 0) {
//            //String search = lerString("um texto para ser pesquisado: ");
//            Palestra pal;
//            int resultados = 0;
//            for (int i = 0; i < ListaPalestras.size(); i++) {
//                pal = ListaPalestras.get(i);
//                texto += Palestra.consultar(pal);
//                if (texto.contains(pesquisa)) {
//                    //System.out.println(texto);
//                    resultados++;
//                }
//            }
//            if (resultados == 0) {
//                mensagem("Nenhum resultado foi encontrado");
//            }
//        } else {
//            mensagem("Não existem palestras cadastradas.\n");
//        }
//        return texto;
//    }
    public static boolean pesquisar() {
        Palestra pal;
        int resultados = 0;
        boolean pesquisou = false;
        if (ListaPalestras.size() > 0) {
            String search = lerString("um texto para ser pesquisado: ");
            for (int i = 0; i < ListaPalestras.size(); i++) {
                pal = ListaPalestras.get(i);
                String texto = consultar(pal);
                texto += "Código: " + ListaPalestras.indexOf(pal);
                if (texto.toLowerCase().contains(search.toLowerCase())) {
                    System.out.println(texto);
                    resultados++;
                    pesquisou = true;
                }
            }
            if (resultados == 0) {
                System.out.println("\nNenhum resultado foi encontrado.\n");
                pesquisou = false;
            }
        } else {
            System.out.println("\nNão existem artigos cadastrados.\n");
            pesquisou = false;
        }

        return pesquisou;
    }

    public static void incluir() {
        String titulo, autor, resumo, abstrato, curriculo;
        int situacao, duracao;

        System.out.println();
        titulo = lerString("o título: ");

        autor = lerString("o autor:");
        resumo = lerString("o resumo:");
        abstrato = lerString("o abstract:");
        curriculo = lerString("o curriculo:");

        do {
            mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado");
            situacao = lerInteiro("a situação: ");
            if (situacao <= 0 || situacao > 3) {
                mensagem("\nVocê digitou um valor inválido.\n");
            }
        } while (situacao <= 0 || situacao > 3);

        do {
            duracao = lerInteiro("a duração em minutos:");
        } while (duracao < 0);

        Palestra palestra = new Palestra(titulo, situacao, autor, resumo, abstrato, duracao, curriculo);
        ListaPalestras.add(palestra);
        mensagem("\nPalestra adicionada com sucesso!\n");
    }

    public static void mensagem(String texto) {
        System.out.print(texto);
    }
}
