package t1rp2.Palestras;

import java.util.Scanner;
//import static t1rp2.PalestrasMetodos.isInteger;
import static t1rp2.Palestras.PalestrasInterface.mensagem;

public class LerVariaveis {

    public static int lerInteiro(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        int integer;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine().trim();
            if (isInteger(var) == false) {
                mensagem("\nVocê digitou um valor inválido.\n");
            }
        } while (isInteger(var) == false);
        integer = Integer.parseInt(var);
        return integer;
    }

    public static String lerString(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine();
        } while (var.trim().equalsIgnoreCase(""));
        return var;
    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static String verificaSituacao(int valor) {
        String situacao = "Sob Avaliação";
        if (valor == 2) {
            situacao = "Aprovado";
        } else if (valor == 3) {
            situacao = "Reprovado";
        }
        return situacao;
    }

    public static String minPHoras(int minutos) {
        String horas;
        int h1, h2;
        h1 = minutos / 60;
        h2 = minutos % 60;
        horas = h1 + ":" + h2;
        if (h1 < 10) {
            horas = "0" + horas;
        }
        if (h2 == 0) {
            horas = horas + "0";
        }
        return horas;
    }

}
