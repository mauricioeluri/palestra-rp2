package t1rp2.Palestras;

import static bruno.ArtigoMetodos.lerInteiro;
import java.util.Scanner;
import static t1rp2.Palestras.PalestrasInterface.mensagem;

public class Base {

    public static String lerString(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine();
        } while (var.trim().equalsIgnoreCase(""));
        return var;

    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static int lerInteiro(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        int integer;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine().trim();
            if (isInteger(var) == false) {
                mensagem("\nVocê digitou um valor inválido.\n");
            }
        } while (isInteger(var) == false);
        integer = Integer.parseInt(var);
        return integer;
    }

    public static String verificaSituacao(int valor) {
        String situacao = "Sob Avaliação";
        if (valor == 2) {
            situacao = "Aprovado";
        } else if (valor == 3) {
            situacao = "Reprovado";
        }
        return situacao;
    }

    public static void menu(String secao) {
        int opcao;
        mensagem("***Bem vindo(a) a seção de " + secao + "***\n");


        do {
            opcao = menuOpcoes();
            switch (opcao) {
                case 1:
                    switch (secao) {
//                        case "artigo":
//                            ArtigoInterface.incluir();
//                            break;

                        //case "minicurso":
                        //MinicursoInterface.incluir();
                        //break;
                        case "palestra":
                        PalestrasInterface.incluir();
                        break;
                        //case "resumo":
                        //ResumoInterface.incluir();
                        //break;
                        //case "monografia":
                        //Monografia.incluir();
                        //break;
                    }
                    break;

                case 2:
                    switch (secao) {
//                        case "artigo":
//                            ArtigoInterface.pesquisar();
//                            break;

                        //case "minicurso":
                        //MinicursoInterface.pesquisar();
                        //break;
                        case "palestra":
                        PalestrasInterface.pesquisar();
                        break;
                        //case "resumo":
                        //ResumoInterface.pesquisar();
                        //break;
                        //case "monografia":
                        //Monografia.pesquisar();
                        //break;
                    }
                    break;

                case 3:
//                    Scanner entrada = new Scanner(System.in);
//                    int resp,
//                     categoria;
//                    String titulo;
//                    Artigo editado;
//                    do {
//                        System.out.print("Digite o código para editar:");
//                        resp = entrada.nextInt();
//                        if (resp >= Artigo.ListaArtigo.size() || resp < 0) {
//                            System.out.println("Você digitou um código inválido.\n");
//                        }
//                    } while (resp >= Artigo.ListaArtigo.size() || resp < 0);
//
//                    System.out.println("\n[1] Nome\n[2] Situação\n[3] Autor\n[4] Instituição\n[5] Palavra-Chave\n[6] Resumo\n[7] Abstract");
//                    System.out.print("Escolha uma categoria para editar:");
//                    categoria = entrada.nextInt();
//
//                    switch (categoria) {
//                        case 1:
//                            entrada.nextLine();
//                            System.out.print("Digite o novo título:");
//                            titulo = entrada.nextLine();
//                            editado = new Artigo(titulo);
//                            Artigo.ListaArtigo.set(resp, editado);
//                    }

                    break;

                case 4:
                    
                    Palestra.excluir();
//                    switch (secao) {

//                        case "artigo":
//                            ArtigoInterface.excluir();
//                            break;

                        //case "minicurso":
                        //MinicursoInterface.excluir();
                        //break;
                        //case "palestra":
                        //PalestraInterface.excluir();
                        //break;
                        //case "resumo":
                        //ResumoInterface.excluir();
                        //break;
                        //case "monografia":
                        //Monografia.excluir();
                        //break;
                        
                        
                        
//                    }
//                    break;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

            }
        } while (opcao != 0);
    }

    public static int menuOpcoes() {
        int opcao;
        do {
            opcao = lerInteiro("\n[1] Incluir\n[2] Consultar\n[3] Editar\n[4] Excluir\n[0] Sair\n");
            if (opcao < 0 || opcao > 4) {
                mensagem("Você digitou uma opção inválida.\n\n");
            }
        } while (opcao < 0 && opcao > 4);
        return opcao;
    }
    
}
