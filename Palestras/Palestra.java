package t1rp2.Palestras;

//import static t1rp2.PalestrasMetodos.minPHoras;
import java.util.Scanner;
import static t1rp2.Palestras.LerVariaveis.minPHoras;
import static t1rp2.bruno.ArtigoMetodos.verificaSituacao;

//import static t1rp2.PalestrasMetodos.verificaSituacao;

public class Palestra {

    private String titulo;
    private int situacao;
    private String autor;
    private String resumo;
    private String abstrato;
    private int duracao;
    private String curriculo;

    public Palestra(String titulo, int situacao, String autor, String resumo, String abstrato, int duracao, String curriculo) {
        this.titulo = titulo;
        this.situacao = situacao;
        this.autor = autor;
        this.resumo = resumo;
        this.abstrato = abstrato;
        this.duracao = duracao;
        this.curriculo = curriculo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getSituacao() {
        return situacao;
    }

    public void setSituacao(int situacao) {
        this.situacao = situacao;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getAbstrato() {
        return abstrato;
    }

    public void setAbstrato(String abstrato) {
        this.abstrato = abstrato;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public String getCurriculo() {
        return curriculo;
    }

    public void setCurriculo(String curriculo) {
        this.curriculo = curriculo;
    }

    public static String consultar(Palestra palestras) {
        String texto;
        texto = "\nTítulo: " + palestras.getTitulo()
                + ";\nSituação: " + verificaSituacao(palestras.getSituacao())
                + ";\nAutor: " + palestras.getAutor()
                + ";\nResumo: " + palestras.getResumo()
                + ";\nAbstract: " + palestras.getAbstrato()
                + ";\nDuração: " + minPHoras(palestras.getDuracao())
                + ";\nCurrículo: " + palestras.getCurriculo()
                + ";\n";
        return texto;
    }
    
    
    
    public static void excluir() {
    	if (t1rp2.Palestras.PalestrasInterface.pesquisar()==true) {
    		Scanner entrada = new Scanner (System.in);
        	int resp;
        	String conf;
        	
        	do {
            	System.out.print("Digite o código para excluir:");
            	resp = entrada.nextInt();
            	if (resp>=PalestrasMetodos.ListaPalestras.size() || resp<0) {
            		System.out.println("Você digitou um código inválido.\n");
            	}
        	} while (resp>=PalestrasMetodos.ListaPalestras.size() || resp<0);
            
        	do {
        		System.out.println("Tem certeza que deseja excluir essa item? s/n");
            	conf = entrada.next();
            	if (!conf.equals("s") && !conf.equals("n")) {
            		System.out.println("Você digitou um valor inválido.");
            	}
            } while (!conf.equals("s") && !conf.equals("n"));	
        	switch (conf) {
        		case "s":
        			PalestrasMetodos.ListaPalestras.remove(resp);
        			break;
        		case "n":
        			System.out.println();
        	      	break;
        	}
    	}
    }
}
